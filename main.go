package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
)

type configFile struct {
	Headers map[string]string `json:"headers,omitempty"`
	Proxy   string            `json:"proxy,omitempty"`
	ZoneID  string            `json:"zoneid"`
}

type zoneRecord struct {
	ID      string `json:"id,omitempty"`
	Name    string `json:"name"`
	Type    string `json:"type"`
	Content string `json:"content"`
	Proxied bool   `json:"proxied"`
	Deleted bool   `json:"-"`
}

type oneRecordResponse struct {
	Succeed bool       `json:"success"`
	Result  zoneRecord `json:"result"`
	Error   []string   `json:"errors,omitempty"`
}

type cfRecordsResponse struct {
	Succeed bool         `json:"success"`
	Result  []zoneRecord `json:"result"`
	Error   []string     `json:"errors,omitempty"`
}

type cloudflareZone struct {
	ID             string
	RecordList     []zoneRecord
	HTTPClient     *http.Client
	DefaultHeaders map[string]string
}

func checkRecordType(recordType string) bool {
	switch recordType {
	case "A", "AAAA", "CNAME", "TXT", "NS":
		return true
	default:
		return false
	}
}

func newCloudflareZone(zoneID string, authHeader map[string]string) (c *cloudflareZone) {
	c = &cloudflareZone{ID: zoneID, DefaultHeaders: authHeader}
	c.HTTPClient = &http.Client{}
	return
}

func (c *cloudflareZone) SetProxy(proxyURL string) (err error) {
	pu, err := url.Parse(proxyURL)
	if err != nil {
		return
	}
	c.HTTPClient.Transport = &http.Transport{Proxy: http.ProxyURL(pu)}
	return
}

func (c *cloudflareZone) createRequest(method, recordID, appendURI string, postJSON interface{}) (res []byte, err error) {
	if recordID != "" {
		appendURI = "/" + recordID + appendURI
	}
	var postDataReader *bytes.Buffer
	if postJSON != nil {
		var postData []byte
		postData, err = json.Marshal(postJSON)
		if err != nil {
			return
		}
		postDataReader = bytes.NewBuffer(postData)
	} else {
		postDataReader = bytes.NewBuffer([]byte(""))
	}
	req, err := http.NewRequest(method, "https://api.cloudflare.com/client/v4/zones/"+c.ID+"/dns_records"+appendURI, postDataReader)
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", "application/json")
	for k, v := range c.DefaultHeaders {
		req.Header.Set(k, v)
	}
	r, err := c.HTTPClient.Do(req)
	if err != nil {
		return
	}
	defer r.Body.Close()
	res, err = ioutil.ReadAll(r.Body)
	return
}

func (c *cloudflareZone) Refesh() (err error) {
	res, err := c.createRequest("GET", "", "?per_page=50000", nil)
	if err != nil {
		return
	}
	var respList cfRecordsResponse
	err = json.Unmarshal(res, &respList)
	if err != nil {
		return
	}
	if !respList.Succeed {
		err = fmt.Errorf(respList.Error[0])
		return
	}
	c.RecordList = respList.Result
	return
}

func (c *cloudflareZone) CreateRecord(hostName, recordType, content string, enableCDN bool) (recordID string, err error) {
	if hostName == "" {
		hostName = "@"
	}
	recordType = strings.ToUpper(recordType)
	if !checkRecordType(recordType) {
		err = fmt.Errorf("only support A,AAAA,CNAME,TXT,NS record.")
		return
	}
	postRecord := zoneRecord{Name: hostName, Type: recordType, Content: content, Proxied: enableCDN}
	res, err := c.createRequest("POST", "", "", postRecord)
	if err != nil {
		return
	}
	var createdRecordResponse oneRecordResponse
	err = json.Unmarshal(res, &createdRecordResponse)
	if err != nil {
		return
	}
	if !createdRecordResponse.Succeed {
		err = fmt.Errorf(createdRecordResponse.Error[0])
		return
	}
	c.RecordList = append(c.RecordList, createdRecordResponse.Result)
	recordID = createdRecordResponse.Result.ID
	return
}

func (c *cloudflareZone) DeleteRecord(recordOrd int) (err error) {
	type delRec struct {
		Result struct {
			ID string `json:"id"`
		} `json:"result"`
	}
	if c.RecordList[recordOrd].Deleted {
		return fmt.Errorf("record %d already marked as deleted", recordOrd)
	}
	res, err := c.createRequest("DELETE", c.RecordList[recordOrd].ID, "", nil)
	if err != nil {
		return
	}
	var r delRec
	err = json.Unmarshal(res, &r)
	if err != nil {
		return
	}
	if r.Result.ID == "" {
		err = fmt.Errorf("server report record not deleted")
		return
	}
	c.RecordList[recordOrd].Deleted = true
	return
}

func (c *cloudflareZone) EditRecord(recordOrd int, hostName, recordType, content string, enableCDN bool) (err error) {
	if c.RecordList[recordOrd].Deleted {
		return fmt.Errorf("can not edit record %d hence it's marked as deleted", recordOrd)
	}
	if hostName == "" {
		hostName = c.RecordList[recordOrd].Name
	}
	if content == "" {
		content = c.RecordList[recordOrd].Content
	}
	if recordType == "" {
		recordType = c.RecordList[recordOrd].Type
	}
	if !checkRecordType(recordType) {
		return fmt.Errorf("only support A,AAAA,CNAME,TXT,NS record.")
	}
	if hostName == c.RecordList[recordOrd].Name &&
		content == c.RecordList[recordOrd].Content &&
		recordType == c.RecordList[recordOrd].Type &&
		enableCDN == c.RecordList[recordOrd].Proxied {
		err = fmt.Errorf("no change to the record")
		return
	}
	patchRecord := zoneRecord{Name: hostName, Type: recordType, Content: content, Proxied: enableCDN}
	res, err := c.createRequest("PATCH", c.RecordList[recordOrd].ID, "", patchRecord)
	if err != nil {
		return
	}
	var patchRecordResponse oneRecordResponse
	err = json.Unmarshal(res, &patchRecordResponse)
	if err != nil {
		return
	}
	if !patchRecordResponse.Succeed {
		err = fmt.Errorf(patchRecordResponse.Error[0])
		return
	}
	c.RecordList[recordOrd] = patchRecordResponse.Result
	return
}

func readStdinLn(promptText string) string {
	fmt.Print(promptText, "> ")
	text, err := bufio.NewReader(os.Stdin).ReadString('\n')
	if err != nil {
		fmt.Println("error reading stdin")
		os.Exit(3)
	}
	return strings.TrimSpace(text)
}

func inputBoolean(promptText string, defaultValue bool) bool {
	promptOptionText := " (yes/NO)"
	if defaultValue {
		promptOptionText = " (YES/no)"
	}
	for {
		switch e := strings.ToUpper(readStdinLn(promptText + promptOptionText)); e {
		case "":
			return defaultValue
		case "Y", "YES", "TRUE", "1":
			return true
		case "N", "NO", "FALSE", "0":
			return false
		default:
			fmt.Println("Unexcepted input text")
		}
	}
}

func inputEnableCDN(recordType string, defVal bool) bool {
	switch recordType {
	case "A", "AAAA", "CNAME":
		return inputBoolean("Enable CDN proxy?", defVal)
	}
	return false
}

func main() {
	var config configFile
	configFilePath := "config.json"
	if len(os.Args) > 1 {
		configFilePath = os.Args[1]
	}
	configFH, err := os.Open(configFilePath)
	if err != nil {
		fmt.Println("can not open config file!")
		os.Exit(1)
	}
	defer configFH.Close()
	configJSONBytes, err := ioutil.ReadAll(configFH)
	if err != nil {
		fmt.Println("config file is not readable")
		os.Exit(1)
	}
	err = json.Unmarshal(configJSONBytes, &config)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	zone := newCloudflareZone(config.ZoneID, config.Headers)
	if config.Proxy != "" {
		err = zone.SetProxy(config.Proxy)
		if err != nil {
			fmt.Println(err)
			os.Exit(2)
		}
	}
	err = zone.Refesh()
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	fmt.Println(len(zone.RecordList), "records loaded")
	inputRecordNumber := func() (num int, err error) {
		num, err = strconv.Atoi(readStdinLn("Record order"))
		if err != nil {
			return
		}
		if num < 0 {
			err = fmt.Errorf("record number cannot less than 0!")
			return
		}
		if num >= len(zone.RecordList) {
			err = fmt.Errorf("record number out of range!")
			return
		}
		return
	}
	for {
		switch command := strings.ToLower(readStdinLn("Command")); command {
		case "":
		case "exit", "quit":
			os.Exit(0)
		case "ref", "refresh":
			err = zone.Refesh()
			if err != nil {
				fmt.Println(err)
			}
		case "ls", "list":
			for k, v := range zone.RecordList {
				if v.Deleted {
					continue
				}
				var cdnText string
				if v.Proxied {
					cdnText = "#"
				}
				fmt.Println(k, " ", v.Name, "\t", v.Type, "\t", v.Content, cdnText)
			}
		case "id":
			recOrd, err := inputRecordNumber()
			if err != nil {
				fmt.Println(err)
				break
			}
			fmt.Println(recOrd, "\t", zone.RecordList[recOrd].ID)
		case "del", "rm", "delete", "remove":
			recOrd, err := inputRecordNumber()
			if err != nil {
				fmt.Println(err)
				break
			}
			err = zone.DeleteRecord(recOrd)
			if err != nil {
				fmt.Println(err)
			}
		case "new", "create":
			hostName := readStdinLn("Hostname")
			recordType := strings.ToUpper(readStdinLn("DNS record type"))
			content := readStdinLn("Content")
			enableCDN := inputEnableCDN(recordType, true)
			recID, err := zone.CreateRecord(hostName, recordType, content, enableCDN)
			if err != nil {
				fmt.Println(err)
				break
			}
			fmt.Println("cerated DNS record orderd", len(zone.RecordList)-1, "with id", recID)
		case "edit":
			recOrd, err := inputRecordNumber()
			if err != nil {
				fmt.Println(err)
				break
			}
			hostName := readStdinLn("Hostname " + zone.RecordList[recOrd].Name)
			recordType := strings.ToUpper(readStdinLn("DNS record type " + zone.RecordList[recOrd].Type))
			if recordType == "" {
				recordType = zone.RecordList[recOrd].Type
			}
			content := readStdinLn("Content " + zone.RecordList[recOrd].Content)
			enableCDN := inputEnableCDN(recordType, zone.RecordList[recOrd].Proxied)
			err = zone.EditRecord(recOrd, hostName, recordType, content, enableCDN)
			if err != nil {
				fmt.Println(err)
			}
		default:
			fmt.Println("Invalid command.")
		}
	}
}
